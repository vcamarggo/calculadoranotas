$(function() {
	$('.js-toggle').bind('click', function(event) {
		$('.js-sidebar, .js-content').toggleClass('is-toggled');
		event.preventDefault();
	});
});

$(document)
		.ready(
				function() {
					if (document.URL.indexOf("home") > -1
							|| document.URL.indexOf("calculadora") > -1) {
						document.getElementById("busca").className = "is-selected";
					} else if (document.URL.indexOf("sobre") > -1) {
						document.getElementById("sobre").className = "is-selected";
					} else if (document.URL.indexOf("ajuda") > -1) {
						document.getElementById("ajuda").className = "is-selected";
					} else if (document.URL.indexOf("login") > -1) {
						document.getElementById("login").className = "is-selected";
					} else if (document.URL.indexOf("cadastro/cursoDisciplina") > -1) {
						document.getElementById("curso-disciplina").className = "is-selected";
					} else if (document.URL.indexOf("cadastro/curso") > -1) {
						document.getElementById("curso").className = "is-selected";
					} else if (document.URL.indexOf("cadastro/disciplina") > -1) {
						document.getElementById("disciplina").className = "is-selected";
					} else if (document.URL.indexOf("adm") > -1) {
						document.getElementById("user").className = "is-selected";
					}

				});
