package br.com.martecs.calcnotas.pessoa;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.martecs.calcnotas.enumeration.Autoridade;

@Entity
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "USU_AUTH", length = 60, nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Autoridade autoridade;

    @Transient
    private String confirmation;

    @Column(name = "USU_ATIV", columnDefinition = "BOOLEAN")
    private Boolean enable = true;

    @Id
    @Column(name = "USU_ID", length = 20)
    private String id;

    @Transient
    private String password;

    @Column(name = "USU_PASS", length = 128)
    private String passwordSHA256;

    public Usuario() {
    }

    public void encriptPassword() throws NoSuchAlgorithmException {

	MessageDigest md = MessageDigest.getInstance("SHA-256");
	md.update(password.getBytes());

	byte byteData[] = md.digest();

	StringBuffer sb = new StringBuffer();
	for (int i = 0; i < byteData.length; i++) {
	    sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	}

	this.setPasswordSHA256(sb.toString());

    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Usuario other = (Usuario) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    public Autoridade getAutoridade() {
	return autoridade;
    }

    public String getConfirmation() {
	return confirmation;
    }

    public Boolean getEnable() {
	return enable;
    }

    public String getId() {
	return id;
    }

    public String getPassword() {
	return password;
    }

    public String getPasswordSHA256() {
	return passwordSHA256;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    public boolean isTamanhoPasswordValido() {
	return getPassword().length() >= 6;
    }

    public void setAutoridade(Autoridade autoridade) {
	this.autoridade = autoridade;
    }

    public void setConfirmation(String confirmation) {
	this.confirmation = confirmation;
    }

    public void setEnable(Boolean enable) {
	this.enable = enable;
    }

    public void setId(String id) {
	this.id = id;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setPasswordSHA256(String passwordSHA256) {
	this.passwordSHA256 = passwordSHA256;
    }

    @Override
    public String toString() {
	return id;
    }

    public Boolean passwordEqualConfirmation() {
	return password.equals(confirmation);
    }

}
