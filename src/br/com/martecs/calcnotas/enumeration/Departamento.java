package br.com.martecs.calcnotas.enumeration;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

public enum Departamento {

    DAC, DAD, DAG, DAU, DBC, DBI, DBQ, DBS, DCC, DCF, DCI, DCM, DCO, DCS, DDM, DDP, DEA, DEC, DEF, DEM, DEN, DEP, DEQ, DES, DET, DFA, DFE, DFI, DFS, DFT, DGE, DHI, DIN, DLE, DMA, DMD, DMU, DMV, DOD, DPI, DPP, DQI, DTP, DZO,;
}
