package br.com.martecs.calcnotas.enumeration;

/**
 * @author V.Camargo
 * 
 * @Date 4 de abr de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
public enum TipoCombinacao {
    OTIMIZADA("Otimizada"), BALANCEADA("Balanceada");

    private final String toString;

    private TipoCombinacao(String toString) {
		this.toString = toString;
	}

    public String toString() {
	return toString;
    }
}
