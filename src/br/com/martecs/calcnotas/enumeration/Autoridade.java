package br.com.martecs.calcnotas.enumeration;

/**
 * @author Vinícius Camargo
 *
 * @date 26/09/2016
 */
public enum Autoridade {

    ROLE_ADMIN, ROLE_GERAL;

}
