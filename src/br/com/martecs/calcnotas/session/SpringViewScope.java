package br.com.martecs.calcnotas.session;

import java.util.Map;

import javax.faces.context.FacesContext;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;

/**
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 *
 */
public class SpringViewScope implements Scope {

    public final static String VIEW_SCOPE_KEY = "view";

    @SuppressWarnings("rawtypes")
    public Object get(String name, ObjectFactory objectFactory) {
	Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();

	if (viewMap.containsKey(name)) {
	    return viewMap.get(name);
	} else {
	    Object object = objectFactory.getObject();
	    viewMap.put(name, object);

	    return object;
	}
    }

    public Object remove(String name) {
	return FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(name);
    }

    public String getConversationId() {
	return null;
    }

    public void registerDestructionCallback(String name, Runnable callback) {
    }

    public Object resolveContextualObject(String key) {
	return null;
    }
}