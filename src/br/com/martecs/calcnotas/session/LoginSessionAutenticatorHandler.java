package br.com.martecs.calcnotas.session;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionException;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import br.com.martecs.calcnotas.controller.SessaoController;
import br.com.martecs.calcnotas.persistence.daoimp.UsuarioDao;
import br.com.martecs.calcnotas.util.ApplicationContextProvider;
import br.com.martecs.calcnotas.web.constants.URLConstants;

public class LoginSessionAutenticatorHandler implements AuthenticationSuccessHandler {

    private final static int MINUTO_SEGUNDO = 60;
    private final static int MINUTOS_SESSION = 20 * MINUTO_SEGUNDO;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	    Authentication authentication) throws IOException, ServletException {

	try {
	    ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
	    UsuarioDao usuarioDao = (UsuarioDao) ctx.getBean("usuarioDao");

	    HttpSession session = request.getSession();
	    User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    session.setAttribute("username", authUser.getUsername());
	    session.setAttribute("authorities", authentication.getAuthorities());
	    session.setAttribute("usuario", usuarioDao.getByUserName(authUser.getUsername()));
	    session.setMaxInactiveInterval(MINUTOS_SESSION);

	    response.setStatus(HttpServletResponse.SC_OK);

	    SessaoController sessaoAtual = (SessaoController) ctx.getBean("sessaoController");
	    sessaoAtual.atualizaAutorizacao(session);

	    redirectStrategy.sendRedirect(request, response, "/" + URLConstants.HOME_URL);

	} catch (SessionException e) {
	    e.printStackTrace();
	}

    }

}
