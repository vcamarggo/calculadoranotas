package br.com.martecs.calcnotas.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 *
 */
public class ApplicationContextProvider implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    /**
     * Loaded during Spring initialization.
     */
    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
	applicationContext = ctx;
    }

    /**
     * Get access to the Spring ApplicationContext from anywhere in application.
     * 
     * @return Spring ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
	return applicationContext;
    }
}
