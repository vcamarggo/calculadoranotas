package br.com.martecs.calcnotas.converter;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import br.com.martecs.calcnotas.persistence.daoimp.DisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.Disciplina;
import br.com.martecs.calcnotas.util.ApplicationContextProvider;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 * 
 */
@FacesConverter("disciplinaConverter")
public class DisciplinaConverter implements Converter {

    @Autowired
    private DisciplinaDao disciplinaDao;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
	Disciplina disciplina = (Disciplina) this.getAttributesFrom(uiComponent).get(value);
	if (disciplina == null && value != null && value != "") {
	    if (disciplinaDao == null) {
		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		disciplinaDao = (DisciplinaDao) ctx.getBean("disciplinaDao");
	    }
	    disciplina = disciplinaDao.searchById(Disciplina.class, Long.valueOf(value));
	}
	return disciplina;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Disciplina && value != null) {
	    Disciplina disciplina = (Disciplina) value;
	    addAttribute(component, disciplina);
	    return disciplina.getDescricao();
	}
	return "";
    }

    /**
     * Armazena o objeto no componente
     * 
     * @param component
     * @param idable
     */
    protected void addAttribute(UIComponent component, Object ob) {
	if (ob != null && ob instanceof Disciplina && ((Disciplina) ob).getDescricao() != null) {
	    Disciplina disciplina = (Disciplina) ob;
	    String key = disciplina.getDescricao();
	    this.getAttributesFrom(component).put(key, ob);
	}
    }

    /**
     * Restaura o objeto do componente
     * 
     * @param component
     * @return
     */
    protected Map<String, Object> getAttributesFrom(UIComponent component) {
	return component.getAttributes();
    }

    public DisciplinaDao getDisciplinaDao() {
	return disciplinaDao;
    }

    public void setDisciplinaDao(DisciplinaDao disciplinaDao) {
	this.disciplinaDao = disciplinaDao;
    }

}