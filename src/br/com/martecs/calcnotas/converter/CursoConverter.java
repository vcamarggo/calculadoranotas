package br.com.martecs.calcnotas.converter;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import br.com.martecs.calcnotas.persistence.daoimp.CursoDao;
import br.com.martecs.calcnotas.persistence.geral.Curso;
import br.com.martecs.calcnotas.util.ApplicationContextProvider;

/**
 * @author V.Camargo
 * 
 * @Date 3 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@FacesConverter("cursoConverter")
public class CursoConverter implements Converter {

    @Autowired
    private CursoDao cursoDao;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
	Curso curso = (Curso) this.getAttributesFrom(uiComponent).get(value);
	if (curso == null && value != null && value != "") {
	    if (cursoDao == null) {
		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		cursoDao = (CursoDao) ctx.getBean("cursoDao");
	    }
	    curso = cursoDao.searchById(Curso.class, Long.valueOf(value));
	}
	return curso;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Curso && value != null) {
	    Curso curso = (Curso) value;
	    addAttribute(component, curso);
	    return curso.getDescricao();
	}
	return "";
    }

    /**
     * Armazena o objeto no componente
     * 
     * @param component
     * @param idable
     */
    protected void addAttribute(UIComponent component, Object ob) {
	if (ob != null && ob instanceof Curso && ((Curso) ob).getDescricao() != null) {
	    Curso curso = (Curso) ob;
	    String key = curso.getDescricao();
	    this.getAttributesFrom(component).put(key, ob);
	}
    }

    /**
     * Restaura o objeto do componente
     * 
     * @param component
     * @return
     */
    protected Map<String, Object> getAttributesFrom(UIComponent component) {
	return component.getAttributes();
    }

    public CursoDao getCursoDao() {
	return cursoDao;
    }

    public void setCursoDao(CursoDao cursoDao) {
	this.cursoDao = cursoDao;
    }

}