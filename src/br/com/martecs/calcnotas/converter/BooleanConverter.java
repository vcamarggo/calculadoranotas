package br.com.martecs.calcnotas.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * @author V.Camargo
 * 
 * @Date 14 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@FacesConverter(value = "booleanConverter")
public class BooleanConverter implements Converter {

    public BooleanConverter() {
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String param) {
	try {
	    return Boolean.parseBoolean(param);
	} catch (Exception exception) {
	    throw new ConverterException(exception);
	}
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object obj) {
	try {
	    if ((obj != null) && ((Boolean) obj)) {
		return "true";
	    } else {
		return "false";
	    }
	} catch (Exception exception) {
	    throw new ConverterException(exception);
	}
    }
}