package br.com.martecs.calcnotas.converter;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import br.com.martecs.calcnotas.persistence.daoimp.UsuarioDao;
import br.com.martecs.calcnotas.pessoa.Usuario;
import br.com.martecs.calcnotas.util.ApplicationContextProvider;

/**
 * @author V.Camargo
 * 
 * @Date 3 de jun de 2016
 * 
 * @category Converter
 */
@FacesConverter("usuarioConverter")
public class UsuarioConverter implements Converter {

    @Autowired
    private UsuarioDao usuarioDao;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
	Usuario usuario = (Usuario) this.getAttributesFrom(uiComponent).get(value);
	if (usuario == null && value != null && value != "") {
	    if (usuarioDao == null) {
		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		usuarioDao = (UsuarioDao) ctx.getBean("usuarioDao");
	    }
	    usuario = usuarioDao.searchById(Usuario.class, value);
	}
	return usuario;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Usuario && value != null) {
	    Usuario usuario = (Usuario) value;
	    addAttribute(component, usuario);
	    return usuario.getId();
	}
	return "";
    }

    /**
     * Armazena o objeto no componente
     * 
     * @param component
     * @param idable
     */
    protected void addAttribute(UIComponent component, Object ob) {
	if (ob != null && ob instanceof Usuario && ((Usuario) ob).getId() != null) {
	    Usuario usuario = (Usuario) ob;
	    String key = usuario.getId();
	    this.getAttributesFrom(component).put(key, ob);
	}
    }

    /**
     * Restaura o objeto do componente
     * 
     * @param component
     * @return
     */
    protected Map<String, Object> getAttributesFrom(UIComponent component) {
	return component.getAttributes();
    }

    public UsuarioDao getUsuarioDao() {
	return usuarioDao;
    }

    public void setUsuarioDao(UsuarioDao usuarioDao) {
	this.usuarioDao = usuarioDao;
    }

}