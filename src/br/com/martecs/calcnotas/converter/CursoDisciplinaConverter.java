package br.com.martecs.calcnotas.converter;

import java.io.IOException;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import br.com.martecs.calcnotas.persistence.daoimp.CursoDisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.Curso;
import br.com.martecs.calcnotas.persistence.geral.CursoDisciplina;
import br.com.martecs.calcnotas.util.ApplicationContextProvider;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@FacesConverter("cursoDisciplinaConverter")
public class CursoDisciplinaConverter implements Converter {

    @Autowired
    private CursoDisciplinaDao cursoDisciplinaDao;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
	CursoDisciplina cursoDisciplina = (CursoDisciplina) this.getAttributesFrom(uiComponent).get(value);
	if (cursoDisciplina == null && value != null && value != "") {
	    if (cursoDisciplinaDao == null) {
		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		cursoDisciplinaDao = (CursoDisciplinaDao) ctx.getBean("cursoDisciplinaDao");
	    }
	    try {
		cursoDisciplina = cursoDisciplinaDao.searchById(CursoDisciplina.class, Long.valueOf(value));
	    } catch (NumberFormatException nfe) {
		String url = "/calcnotas/" + URLConstants.HOME_URL;
		try {
		    FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}
	return cursoDisciplina;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Curso && value != null) {
	    Curso cursoDisciplina = (Curso) value;
	    addAttribute(component, cursoDisciplina);
	    return cursoDisciplina.getDescricao();
	}
	return "";
    }

    /**
     * Armazena o objeto no componente
     * 
     * @param component
     * @param idable
     */
    protected void addAttribute(UIComponent component, Object ob) {
	if (ob != null && ob instanceof Curso && ((Curso) ob).getDescricao() != null) {
	    Curso cursoDisciplina = (Curso) ob;
	    String key = cursoDisciplina.getDescricao();
	    this.getAttributesFrom(component).put(key, ob);
	}
    }

    /**
     * Restaura o objeto do componente
     * 
     * @param component
     * @return
     */
    protected Map<String, Object> getAttributesFrom(UIComponent component) {
	return component.getAttributes();
    }

    public CursoDisciplinaDao getCursoDao() {
	return cursoDisciplinaDao;
    }

    public void setCursoDao(CursoDisciplinaDao cursoDisciplinaDao) {
	this.cursoDisciplinaDao = cursoDisciplinaDao;
    }

}