package br.com.martecs.calcnotas.web.constants;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * @author V.Camargo
 * 
 * @Date 2 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@ApplicationScoped
@ManagedBean(name = "urlConstants")
public class URLConstants {

    
    public static final String CONTEXT = "/calcnotas";
    
    public static final String HOME_URL = "home";
    private final String homeURL = HOME_URL;

    public static final String CURSO_LIST_URL = "/pages/cadastro/curso/cursoList.jsf";
    private final String cursoListURL = CURSO_LIST_URL;

    public static final String CURSO_FORM_URL = "/pages/cadastro/curso/cursoForm.jsf";
    private final String cursoFormURL = CURSO_FORM_URL;

    public static final String DISCIPLINA_LIST_URL = "/pages/cadastro/disciplina/disciplinaList.jsf";
    private final String disciplinaListURL = DISCIPLINA_LIST_URL;

    public static final String DISCIPLINA_FORM_URL = "/pages/cadastro/disciplina/disciplinaForm.jsf";
    private final String disciplinaFormURL = DISCIPLINA_FORM_URL;

    public static final String CURSODISCIPLINA_FORM_URL = "/pages/cadastro/cursoDisciplina/cursoDisciplinaForm.jsf";
    private final String cursoDisciplinaFormURL = CURSODISCIPLINA_FORM_URL;

    public static final String CURSODISCIPLINA_LIST_URL = "/pages/cadastro/cursoDisciplina/cursoDisciplinaList.jsf";
    private final String cursoDisciplinaListURL = CURSODISCIPLINA_LIST_URL;

    public static final String USUARIO_FORM_URL = "/pages/adm/usuarioForm.jsf";
    private final String usuarioFormURL = USUARIO_FORM_URL;
    
    public static final String USUARIO_LIST_URL = "/pages/adm/usuarioList.jsf";
    private final String usuarioListURL = USUARIO_LIST_URL;
    
    public static final String CALCULADORA_FORM_URL = "/pages/calculadora.jsf";
    private final String calculadoraFormURL = CALCULADORA_FORM_URL;

    public static final String LOGIN_URL = "login";
    private final String loginURL = LOGIN_URL;

    public static final String ABOUT_URL = "about";
    private final String aboutURL = ABOUT_URL;

    public static final String HELP_URL = "help";
    private final String helpURL = HELP_URL;

    public static String getHomeUrl() {
	return HOME_URL;
    }

    public String getHomeURL() {
	return homeURL;
    }

    public static String getCursoListUrl() {
	return CURSO_LIST_URL;
    }

    public String getCursoListURL() {
	return cursoListURL;
    }

    public static String getCursoFormUrl() {
	return CURSO_FORM_URL;
    }

    public String getCursoFormURL() {
	return cursoFormURL;
    }

    public String getDisciplinaListURL() {
	return disciplinaListURL;
    }

    public String getDisciplinaFormURL() {
	return disciplinaFormURL;
    }

    public String getCursoDisciplinaFormURL() {
	return cursoDisciplinaFormURL;
    }

    public String getCursoDisciplinaListURL() {
	return cursoDisciplinaListURL;
    }

    public String getCalculadoraFormURL() {
	return calculadoraFormURL;
    }

    public String getLoginURL() {
	return loginURL;
    }

    public String getAboutURL() {
	return aboutURL;
    }

    public String getHelpURL() {
	return helpURL;
    }

    public String getUsuarioFormURL() {
        return usuarioFormURL;
    }

    public String getUsuarioListURL() {
        return usuarioListURL;
    }

}