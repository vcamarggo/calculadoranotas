package br.com.martecs.calcnotas.web.constants;

/**
 * @author V.Camargo
 * 
 * @Date 3 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ApplicationScoped
@ManagedBean(name = "imageConstants")
public class ImageConstants {

    public final static String ENABLE_ICON = "/imagens/enableIcon.png";
    private final String enableIcon = ENABLE_ICON;

    public final static String DISABLE_ICON = "/imagens/enableIcon.png";
    private final String disableIcon = DISABLE_ICON;

    public final static String MINUS_ICON = "/imagens/minusIcon.png";
    private final String minusIcon = MINUS_ICON;

    public final static String ADD_ICON = "/imagens/addIcon.png";
    private final String addIcon = ADD_ICON;

    public final static String SAVE_ICON = "/imagens/saveIcon.png";
    private final String saveIcon = SAVE_ICON;

    public final static String EDIT_ICON = "/imagens/editIcon.png";
    private final String editIcon = EDIT_ICON;

    public final static String DELETE_ICON = "/imagens/deleteIcon.png";
    private final String deleteIcon = DELETE_ICON;

    public final static String ACTIVE_ICON = "/imagens/activeIcon.png";
    private final String activeIcon = ACTIVE_ICON;

    public final static String INACTIVE_ICON = "/imagens/inactiveIcon.png";
    private final String inactiveIcon = INACTIVE_ICON;

    public final static String CONFIRM_ICON = "/imagens/confirmIcon.png";
    private final String confirmIcon = CONFIRM_ICON;

    public final static String NOT_CONFIRM_ICON = "/imagens/notConfirmIcon.png";
    private final String notConfirmIcon = NOT_CONFIRM_ICON;

    public final static String REFRESH_ICON = "/imagens/refreshIcon.png";
    private final String refreshIcon = REFRESH_ICON;

    public final static String CLOSE_ICON = "/imagens/closeIcon.png";
    private final String closeIcon = CLOSE_ICON;

    public final static String COMBINACOES_ICON = "/imagens/combinacoesIcon.png";
    private final String combinacoesIcon = COMBINACOES_ICON;

    public final static String LOGIN_ICON = "/imagens/loginIcon.png";
    private final String loginIcon = LOGIN_ICON;

    public final static String FORBIDDEN_PAGE = "/imagens/forbiddenPage.png";
    private final String forbiddenPage = FORBIDDEN_PAGE;

    public final static String LOGO = "/imagens/logo.png";
    private final String logo = LOGO;

    public final static String LOGO_ICO = "/imagens/icon.ico";
    private final String logoICO = LOGO_ICO;

    public String getEnableIcon() {
	return enableIcon;
    }

    public String getDisableIcon() {
	return disableIcon;
    }

    public String getAddIcon() {
	return addIcon;
    }

    public String getEditIcon() {
	return editIcon;
    }

    public String getDeleteIcon() {
	return deleteIcon;
    }

    public String getActiveIcon() {
	return activeIcon;
    }

    public String getInactiveIcon() {
	return inactiveIcon;
    }

    public String getConfirmIcon() {
	return confirmIcon;
    }

    public String getNotConfirmIcon() {
	return notConfirmIcon;
    }

    public String getSaveIcon() {
	return saveIcon;
    }

    public String getForbiddenPage() {
	return forbiddenPage;
    }

    public String getLogo() {
	return logo;
    }

    public String getLoginIcon() {
	return loginIcon;
    }

    public String getCombinacoesIcon() {
	return combinacoesIcon;
    }

    public String getLogoICO() {
	return logoICO;
    }

    public String getRefreshIcon() {
	return refreshIcon;
    }

    public String getMinusIcon() {
	return minusIcon;
    }

    public String getCloseIcon() {
	return closeIcon;
    }

}
