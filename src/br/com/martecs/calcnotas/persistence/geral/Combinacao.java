package br.com.martecs.calcnotas.persistence.geral;

/**
 * @author V.Camargo
 * 
 * @Date 30 de mar de 2016
 */

public class Combinacao implements Comparable<Combinacao> {

    public Combinacao() {
    }

    public Combinacao(Avaliacao avaliacao, Integer nrOrdem) {
	this.peso = avaliacao.getPeso();
	this.porcentagemSub = avaliacao.getPorcentagemSub();
	this.nome = avaliacao.getDescricao();
	if (avaliacao.getNota() == "") {
	    this.notaSugerida = 0L;
	} else {
	    this.notaSugerida = Long.parseLong(avaliacao.getNota());
	}
	this.nrOrdem = nrOrdem;
    }

    private Double peso;
    private Double porcentagemSub;
    private String nome;
    private Integer nrOrdem;
    private Long notaSugerida;

    public Double getTotal() {
	if (notaSugerida != null && notaSugerida > 0)
	    return notaSugerida * (peso * porcentagemSub);
	else
	    return 0D;
    }

    public Integer getNrOrdem() {
	return nrOrdem;
    }

    public void setNrOrdem(Integer nrOrdem) {
	this.nrOrdem = nrOrdem;
    }

    @Override
    public int compareTo(Combinacao o) {
	if (this.getNrOrdem() < o.getNrOrdem()) {
	    return -1;
	} else {
	    return 1;
	}
    }

    public Double getPeso() {
	return peso;
    }

    public void setPeso(Double peso) {
	this.peso = peso;
    }

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public Long getNotaSugerida() {
	return notaSugerida;
    }

    public void setNotaSugerida(Long notaSugerida) {
	this.notaSugerida = notaSugerida;
    }

    public Double getPorcentagemSub() {
	return porcentagemSub;
    }

    public void setPorcentagemSub(Double porcentagemSub) {
	this.porcentagemSub = porcentagemSub;
    }

}
