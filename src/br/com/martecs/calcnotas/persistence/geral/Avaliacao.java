package br.com.martecs.calcnotas.persistence.geral;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Entity
public class Avaliacao implements Serializable, Cloneable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "AVA_ID", length = 8)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "AVA_DESC", length = 256)
    @NotEmpty
    private String descricao;

    @Column(name = "AVA_PORCENTAGEMSUB", precision = 2)
    @NotNull
    private Double sub = new Double(100);

    @Column(name = "AVA_PESO", precision = 2)
    @NotNull
    private Double peso = new Double(1);

    @Column(name = "AVA_PESOR", precision = 2)
    @NotNull
    private Double pesoRelativo = new Double(1);

    @ManyToOne
    @JoinColumn(name = "AVA_CURSODISCIPLINA", referencedColumnName = "CD_COD")
    private CursoDisciplina cursoDisciplina;

    @Transient
    private String nota;

    @Transient
    private Long total;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public Double getPeso() {
	return peso;
    }

    public void setPeso(Double peso) {
	this.peso = peso;
    }

    public CursoDisciplina getCursoDisciplina() {
	return cursoDisciplina;
    }

    public void setCursoDisciplina(CursoDisciplina cursoDisciplina) {
	this.cursoDisciplina = cursoDisciplina;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Avaliacao other = (Avaliacao) obj;
	if (descricao == null) {
	    if (other.descricao != null)
		return false;
	} else if (!descricao.equals(other.descricao))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    public Avaliacao clone() {
	Avaliacao av = new Avaliacao();
	av.setDescricao(this.descricao);
	av.setPeso(this.peso);
	av.setPesoRelativo(this.pesoRelativo);
	av.setSub(this.sub);
	return av;
    }

    public String getNota() {
	return nota;
    }

    public void setNota(String nota) {
	this.nota = nota;
    }

    public Double getTotal() {
	if (nota != null && nota != "" && Long.parseLong(nota) > 0)
	    return Long.parseLong(nota) * pesoRelativo;
	else
	    return 0D;
    }

    public Double getPesoRelativo() {
	return pesoRelativo;
    }

    public void setPesoRelativo(Double pesoRelativo) {
	this.pesoRelativo = pesoRelativo;
    }

    public void atualizaPesoRelativo(Double pesoTotal) {
	setPesoRelativo(((getPeso() * (getPorcentagemSub())) / pesoTotal * 100) / 100d);
    }

    public void setTotal(Long total) {
	this.total = total;
    }

    public Double getPorcentagemSub() {
	return sub / 100;
    }

    public Double getSub() {
	return sub;
    }

    public void setSub(Double sub) {
	this.sub = sub;
    }

}
