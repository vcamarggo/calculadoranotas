package br.com.martecs.calcnotas.persistence.geral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Entity
public class CursoDisciplina implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CD_COD", length = 8, nullable = false)
    private Long cod;

    @ManyToOne
    @JoinColumn(name = "CD_CURSO", referencedColumnName = "CUR_ID")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "CD_DISCPLINA", referencedColumnName = "DIS_ID")
    private Disciplina disciplina;

    @OneToMany(mappedBy = "cursoDisciplina", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Avaliacao> avaliacoes;
    
    @Column(name = "CD_USER", length = 256)
    private String usuarioResponsavel;

    public Curso getCurso() {
	return curso;
    }

    public void setCurso(Curso curso) {
	this.curso = curso;
    }

    public Disciplina getDisciplina() {
	return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
	this.disciplina = disciplina;
    }

    public List<Avaliacao> getAvaliacoes() {
	return avaliacoes;
    }

    public void setAvaliacoes(List<Avaliacao> avaliacoes) {
	for (Avaliacao avaliacao : avaliacoes) {
	    avaliacao.setCursoDisciplina(this);
	}
	this.avaliacoes = avaliacoes;
    }

    public CursoDisciplina clone() {
	CursoDisciplina cd = new CursoDisciplina();
	cd.setDisciplina(this.disciplina);
	cd.setCurso(this.curso);
	List<Avaliacao> avaliacoesClonadas = new ArrayList<Avaliacao>();

	for (Avaliacao avaliacao : avaliacoes) {
	    Avaliacao av = avaliacao.clone();
	    avaliacoesClonadas.add(av);
	}

	cd.setAvaliacoes(avaliacoesClonadas);

	return cd;
    }

    public void atualizaPesos() {
	Double pesoTotal = 0D;
	for (Avaliacao avaliacao : avaliacoes) {
	    pesoTotal += avaliacao.getPeso() * avaliacao.getPorcentagemSub();
	}
	for (Avaliacao avaliacao : avaliacoes) {
	    avaliacao.atualizaPesoRelativo(pesoTotal);
	}
    }

    @Override
    public String toString() {
	String toStr = cod + " - " + curso.getDescricao() + " - " + disciplina.getDescricao();
	return toStr;
    }

    public Long getCod() {
	return cod;
    }

    public void setCod(Long cod) {
	this.cod = cod;
    }

    public String getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    public void setUsuarioResponsavel(String usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }


}
