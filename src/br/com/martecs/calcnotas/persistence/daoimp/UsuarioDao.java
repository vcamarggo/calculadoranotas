package br.com.martecs.calcnotas.persistence.daoimp;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import br.com.martecs.calcnotas.persistence.dao.AbstractDao;
import br.com.martecs.calcnotas.pessoa.Usuario;

/**
 * @author Vinícius Camargo
 *
 * @date 14/08/2016
 */
@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UsuarioDao extends AbstractDao<Usuario> {

    private static final long serialVersionUID = 1L;

    @Override
    public List<Usuario> getListAll() {
	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
	CriteriaQuery<Usuario> query = builder.createQuery(Usuario.class);
	@SuppressWarnings("unused")
	Root<Usuario> root = query.from(Usuario.class);
	return entityManager.createQuery(query).getResultList();
    }

    public Usuario getByUserName(String username) throws NoResultException {
	String SQL = ("SELECT u FROM Usuario u WHERE u.id = :_username");
	TypedQuery<Usuario> q = entityManager.createQuery(SQL, Usuario.class);
	q.setParameter("_username", username);
	q.setMaxResults(1);
	Usuario usuario = null;

	try {
	    usuario = q.getSingleResult();
	    return usuario;
	} catch (NoResultException nre) {
	    return null;
	}
    }

}
