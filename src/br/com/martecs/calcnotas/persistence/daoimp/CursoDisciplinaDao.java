package br.com.martecs.calcnotas.persistence.daoimp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import br.com.martecs.calcnotas.persistence.dao.AbstractDao;
import br.com.martecs.calcnotas.persistence.geral.CursoDisciplina;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Repository("cursoDisciplinaDao")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CursoDisciplinaDao extends AbstractDao<CursoDisciplina> {

    private static final long serialVersionUID = 1L;

    @Override
    public List<CursoDisciplina> getListAll() {
	return entityManager.createQuery(
		"SELECT DISTINCT c FROM CursoDisciplina c join fetch c.avaliacoes ORDER BY c.curso.descricao, c.disciplina.descricao",
		CursoDisciplina.class).getResultList();
    }

    public boolean existsEqual(CursoDisciplina cursoDisciplina) throws NoResultException {
	String SQL = ("SELECT cd FROM CursoDisciplina cd WHERE cd.curso =" + " :curso AND cd.disciplina= :disciplina");
	Query q = entityManager.createQuery(SQL);
	q.setParameter("curso", cursoDisciplina.getCurso());
	q.setParameter("disciplina", cursoDisciplina.getDisciplina());
	return q.getResultList().size() > 0;
    }

    public List<CursoDisciplina> getByFilter(String keyWordBusca) {
	TypedQuery<CursoDisciplina> q = null;
	Set<CursoDisciplina> resultadoSQL = new HashSet<CursoDisciplina>();

	String cursosSQL = ("SELECT c FROM Curso c WHERE c.enable = true AND c.descricao LIKE :cursoNome");
	Query q1 = entityManager.createQuery(cursosSQL);
	q1.setParameter("cursoNome", "%" + keyWordBusca + "%");

	if (q1.getResultList().size() > 0) {
	    String cursoDisciplinaCursos = "SELECT cd FROM CursoDisciplina cd join fetch cd.avaliacoes WHERE cd.curso IN :cursos";
	    q = entityManager.createQuery(cursoDisciplinaCursos, CursoDisciplina.class);
	    q.setParameter("cursos", q1.getResultList());
	    resultadoSQL.addAll(q.getResultList());
	}

	String disciplinasSQL = ("SELECT d FROM Disciplina d WHERE  d.descricao LIKE :disciplinaNome");
	Query q2 = entityManager.createQuery(disciplinasSQL);
	q2.setParameter("disciplinaNome", "%" + keyWordBusca + "%");

	if (q2.getResultList().size() > 0) {
	    String cursoDisciplinaDisciplinas = "SELECT cd FROM CursoDisciplina cd join fetch cd.avaliacoes WHERE cd.disciplina IN :disciplinas";
	    q = entityManager.createQuery(cursoDisciplinaDisciplinas, CursoDisciplina.class);
	    q.setParameter("disciplinas", q2.getResultList());
	    resultadoSQL.addAll(q.getResultList());
	}

	return new ArrayList<CursoDisciplina>(resultadoSQL);
    }

    public List<String> filtraPorKeyWord(String keyWordBusca) {

	String SQL = ("SELECT d.curso.descricao FROM CursoDisciplina d WHERE d.curso.descricao LIKE :sugestao");
	TypedQuery<String> q1 = entityManager.createQuery(SQL, String.class).setMaxResults(4);
	q1.setParameter("sugestao", "%" + keyWordBusca + "%");

	Long cod;
	try {
	    cod = Long.parseLong(keyWordBusca);
	} catch (NumberFormatException nfe) {
	    cod = null;
	}
	SQL = ("SELECT d.disciplina.descricao FROM CursoDisciplina d "
		+ "WHERE ((d.disciplina.descricao LIKE :sugestao) OR (d.disciplina.descricaoCurta LIKE :sugestao) OR (d.cod = :cod))");
	TypedQuery<String> q2 = entityManager.createQuery(SQL, String.class).setMaxResults(4);
	q2.setParameter("sugestao", "%" + keyWordBusca + "%");
	q2.setParameter("cod", cod);

	Set<String> setSugestoes = new HashSet<String>();
	setSugestoes.addAll(q1.getResultList());
	setSugestoes.addAll(q2.getResultList());
	return new ArrayList<String>(setSugestoes);
    }

}