package br.com.martecs.calcnotas.persistence.daoimp;

import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import br.com.martecs.calcnotas.persistence.dao.AbstractDao;
import br.com.martecs.calcnotas.persistence.geral.Disciplina;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Repository("disciplinaDao")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DisciplinaDao extends AbstractDao<Disciplina> {

    private static final long serialVersionUID = 1L;

    @Override
    public List<Disciplina> getListAll() {
	return entityManager.createQuery("SELECT c FROM Disciplina c ORDER BY c.descricao", Disciplina.class)
		.getResultList();
    }

}
