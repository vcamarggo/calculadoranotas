package br.com.martecs.calcnotas.persistence.daoimp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import br.com.martecs.calcnotas.persistence.dao.AbstractDao;
import br.com.martecs.calcnotas.persistence.geral.Curso;
import br.com.martecs.calcnotas.persistence.geral.Disciplina;

/**
 * @author V.Camargo
 * 
 * @Date 1 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
@Repository("cursoDao")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CursoDao extends AbstractDao<Curso> {

    private static final long serialVersionUID = 1L;

    @Override
    public List<Curso> getListAll() {
	return entityManager.createQuery("SELECT c FROM Curso c ORDER BY c.descricao", Curso.class).getResultList();
    }

    public List<Disciplina> getListByDisciplinas(String nomeDisciplina) {
	String SQL = "SELECT c FROM Curso c WHERE c.disciplinas IN (SELECT d FROM Disciplina d WHERE d.descricao = :_nomeDisciplina)";
	TypedQuery<Disciplina> q = entityManager.createQuery(SQL, Disciplina.class);
	q.setParameter("_nomeDisciplina", nomeDisciplina);
	List<Disciplina> disciplinas = q.getResultList();
	return disciplinas;
    }

    public List<Curso> getListAllEnable() {
	return entityManager
		.createQuery("SELECT c FROM Curso c WHERE c.enable = true ORDER BY c.descricao", Curso.class)
		.getResultList();
    }

}
