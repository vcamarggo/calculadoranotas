package br.com.martecs.calcnotas.persistence.dao;

import java.util.List;

/**
 * @author Camargo
 *
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
public interface DAO<T> {

    public T save(T object);

    public T update(T object);

    public T delete(T object) throws Exception;

    public T searchById(Class<T> clazz, Object id);

    public List<T> getListAll();

    public List<T> searchAllOrderBy(Class<T> clazz, String orderBy);

}
