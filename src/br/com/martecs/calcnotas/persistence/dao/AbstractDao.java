package br.com.martecs.calcnotas.persistence.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

/**
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public abstract class AbstractDao<T> implements DAO<T>, Serializable {

    private static final long serialVersionUID = 1L;

    @PersistenceContext
    protected EntityManager entityManager;

    @Transactional
    public T save(T object) {
	entityManager.persist(object);
	entityManager.flush();
	return null;
    }

    @Transactional
    public T update(T object) {
	object = entityManager.merge(object);
	entityManager.flush();
	return object;
    }

    @Transactional
    public T delete(T object) throws Exception {
	object = entityManager.merge(object);
	entityManager.remove(object);
	entityManager.flush();
	return null;
    }

    public T refresh(Class<T> clazz, Object id) {
	T object = entityManager.find(clazz, id);
	entityManager.refresh(object);
	return object;
    }

    @Override
    public T searchById(Class<T> clazz, Object id) {
	return (T) entityManager.find(clazz, id);
    }

    @Override
    public List<T> searchAllOrderBy(Class<T> clazz, String orderBy) {
	return entityManager
		.createQuery("SELECT ob FROM " + clazz.getName() + "  ob ORDER BY ob." + orderBy + " DESC", clazz)
		.getResultList();
    }

}
