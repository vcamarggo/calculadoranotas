package br.com.martecs.calcnotas.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.controller.interfaces.ControllerForm;
import br.com.martecs.calcnotas.persistence.daoimp.CursoDao;
import br.com.martecs.calcnotas.persistence.daoimp.CursoDisciplinaDao;
import br.com.martecs.calcnotas.persistence.daoimp.DisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.Avaliacao;
import br.com.martecs.calcnotas.persistence.geral.Curso;
import br.com.martecs.calcnotas.persistence.geral.CursoDisciplina;
import br.com.martecs.calcnotas.persistence.geral.Disciplina;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 1 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("cursoDisciplinaFormController")
public class CursoDisciplinaFormController implements Serializable, ControllerForm {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CursoDao cursoDao;
    private List<Curso> cursos;

    @Autowired
    private DisciplinaDao disciplinaDao;
    private List<Disciplina> disciplinas;

    @Autowired
    private CursoDisciplinaDao cursoDisciplinaDao;
    private CursoDisciplina cursoDisciplina;

    private List<Avaliacao> avaliacoes;
    private Avaliacao avaliacao;

    private Boolean isCopia;

    private Boolean isAlteracao;

    public CursoDisciplinaFormController() {
    }

    @Override
    public void inicializar() {
	if (cursoDisciplina == null) {
	    cursoDisciplina = new CursoDisciplina();
	    avaliacoes = new ArrayList<Avaliacao>();
	} else {
	    if (isCopia) {
		cursoDisciplina = cursoDisciplina.clone();
	    }
	    if (cursoDisciplina.getAvaliacoes().size() > 0) {
		avaliacoes = cursoDisciplina.getAvaliacoes();
	    }
	}
	carregaListaCursos();
	carregaListaDisciplinas();
	avaliacao = null;

    }

    public void selecionar(Avaliacao avaliacao) {
	this.avaliacao = avaliacao;
    }

    private void carregaListaCursos() {
	cursos = cursoDao.getListAllEnable();
    }

    private void carregaListaDisciplinas() {
	disciplinas = disciplinaDao.getListAll();
    }

    public void novaAvaliacao() {
	avaliacao = new Avaliacao();
    }

    public void adicionarAvaliacao() {
	if (!avaliacoes.contains(avaliacao)) {
	    avaliacoes.add(avaliacao);
	}
    }

    public void removeAvaliacao() {
	avaliacoes.remove(avaliacao);
    }

    @Override
    public String save() throws NoSuchAlgorithmException {
	cursoDisciplina.setAvaliacoes(avaliacoes);
	if (cursoDisciplina.getAvaliacoes().size() <= 0) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageController.MSG_AVALIACAO_VAZIA, ""));
	    return null;
	} else if (!isAlteracao && cursoDisciplinaJaCadastrado()) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageController.MSG_REG_DUP, ""));
	    return null;
	} else {
	    isAlteracao = false;
	    cursoDisciplina.atualizaPesos();
	    try {
		cursoDisciplina.setUsuarioResponsavel(
			((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		cursoDisciplinaDao.update(cursoDisciplina);
	    } catch (Exception e) {
		FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageController.MSG_INCL_ERR_INTEGRIDADE, ""));
		e.printStackTrace();
		return null;
	    }
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_INFO, MessageController.MSG_SAVE_SUCCESS, ""));
	    return URLConstants.CURSODISCIPLINA_LIST_URL;
	}
    }

    private boolean cursoDisciplinaJaCadastrado() {
	return cursoDisciplinaDao.searchById(CursoDisciplina.class, cursoDisciplina.getCod()) != null;
    }

    public List<Curso> getCursos() {
	return cursos;
    }

    public void setCursos(List<Curso> cursos) {
	this.cursos = cursos;
    }

    public List<Disciplina> getDisciplinas() {
	return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
	this.disciplinas = disciplinas;
    }

    public CursoDisciplina getCursoDisciplina() {
	return cursoDisciplina;
    }

    public void setCursoDisciplina(CursoDisciplina cursoDisciplina) {
	this.cursoDisciplina = cursoDisciplina;
    }

    public CursoDisciplinaDao getCursoDisciplinaDao() {
	return cursoDisciplinaDao;
    }

    public List<Avaliacao> getAvaliacoes() {
	return avaliacoes;
    }

    public void setAvaliacoes(List<Avaliacao> avaliacoes) {
	this.avaliacoes = avaliacoes;
    }

    public Avaliacao getAvaliacao() {
	return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
	this.avaliacao = avaliacao;
    }

    public Boolean getIsCopia() {
	return isCopia;
    }

    public void setIsCopia(Boolean isCopia) {
	this.isCopia = isCopia;
    }

    public Boolean getIsAlteracao() {
	return isAlteracao;
    }

    public void setIsAlteracao(Boolean isAlteracao) {
	this.isAlteracao = isAlteracao;
    }

}
