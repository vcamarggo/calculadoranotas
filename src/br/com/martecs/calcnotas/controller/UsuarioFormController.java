package br.com.martecs.calcnotas.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.hibernate.SessionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.persistence.daoimp.UsuarioDao;
import br.com.martecs.calcnotas.pessoa.Usuario;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author Vinícius Camargo
 *
 * @date 14/08/2016
 */
@Controller("usuarioFormController")
@Scope(SpringViewScope.VIEW_SCOPE_KEY)
public class UsuarioFormController implements Serializable {

    private static final long serialVersionUID = 1L;
    @Autowired
    private UsuarioDao usuarioDao;
    private List<Usuario> usuarioList;
    private Usuario usuario;

    public static final String PID = "USUARIO";

    public void inicializar() throws SessionException {
	if (usuario == null) {
	    this.usuario = new Usuario();
	}
    }

    public String save() throws NoSuchAlgorithmException, SessionException {
	if (!usuario.isTamanhoPasswordValido()) {
	    MessageController.geraMensagem(FacesMessage.SEVERITY_ERROR,
		    MessageController.MSG_SENHA_VALID_ERR_SENHA_MINIMO_DIGITOS);
	    return null;
	} else {
	    if (!usuario.passwordEqualConfirmation()) {
		MessageController.geraMensagem(FacesMessage.SEVERITY_ERROR,
			MessageController.MSG_SENHA_VALID_ERR_CONFIRMACAO);
		return null;
	    } else if (usuarioDao.getByUserName(usuario.getId()) != null && usuario.getPasswordSHA256() == null) {
		MessageController.geraMensagem(FacesMessage.SEVERITY_ERROR, MessageController.MSG_USUARIO_EXISTENTE);
		return null;
	    }
	    usuario.encriptPassword();
	    try {
		usuarioDao.update(usuario);
	    } catch (Exception e) {
		e.printStackTrace();
		return null;
	    }
	}
	MessageController.geraMensagem(FacesMessage.SEVERITY_INFO, MessageController.MSG_SAVE_SUCCESS);
	return URLConstants.USUARIO_LIST_URL;
    }

    public UsuarioDao getUsuarioDao() {
	return usuarioDao;
    }

    public void setUsuarioDao(UsuarioDao usuarioDao) {
	this.usuarioDao = usuarioDao;
    }

    public List<Usuario> getUsuarioList() {
	return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
	this.usuarioList = usuarioList;
    }

    public Usuario getUsuario() {
	return usuario;
    }

    public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
    }
}