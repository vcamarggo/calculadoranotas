package br.com.martecs.calcnotas.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.controller.interfaces.ControllerForm;
import br.com.martecs.calcnotas.persistence.daoimp.DisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.Disciplina;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 7 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 * 
 */

@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("disciplinaFormController")
public class DisciplinaFormController implements Serializable, ControllerForm {

    private static final long serialVersionUID = 1L;

    @Autowired
    private DisciplinaDao disciplinaDao;

    private Disciplina disciplina;

    public DisciplinaFormController() {
    }

    @Override
    public void inicializar() {
	if (disciplina == null) {
	    disciplina = new Disciplina();
	}
    }

    @Override
    public String save() throws NoSuchAlgorithmException {
	try {
	    disciplinaDao.update(disciplina);
	} catch (Exception e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageController.MSG_INCL_ERR_INTEGRIDADE, ""));
	    e.printStackTrace();
	    return null;
	}
	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage(FacesMessage.SEVERITY_INFO, MessageController.MSG_SAVE_SUCCESS, ""));
	return URLConstants.DISCIPLINA_LIST_URL;
    }

    public DisciplinaDao getDisciplinaDao() {
	return disciplinaDao;
    }

    public Disciplina getDisciplina() {
	return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
	this.disciplina = disciplina;
    }

}
