package br.com.martecs.calcnotas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.controller.interfaces.ControllerList;
import br.com.martecs.calcnotas.persistence.daoimp.DisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.Disciplina;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 3 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("disciplinaController")
public class DisciplinaController implements Serializable, ControllerList {

    private static final long serialVersionUID = 1L;

    @Autowired
    private DisciplinaDao disciplinaDao;

    private Disciplina disciplina;

    private List<Disciplina> disciplinaList;

    @PostConstruct
    @Override
    public void inicializar() {
	list();
    }

    @Override
    public void novo() throws IOException {
	String url = "/calcnotas" + URLConstants.DISCIPLINA_FORM_URL;
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    @Override
    public void editar() throws IOException {
	String url = "/calcnotas" + URLConstants.DISCIPLINA_FORM_URL + "?disciplina=" + disciplina.getId();
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public void delete() {
	try {
	    disciplinaDao.delete(disciplina);
	    list();
	} catch (Exception e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageController.MSG_EXCL_ERR_INTEGRIDADE, ""));
	    e.printStackTrace();
	    return;
	}
	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage(FacesMessage.SEVERITY_INFO, MessageController.MSG_EXCL_SUCCESS, ""));
    }

    public void list() {
	this.disciplinaList = disciplinaDao.getListAll();
    }

    /*----------------GETTERS & SETTERS----------------------*/

    public Disciplina getDisciplina() {
	return disciplina;
    }

    public DisciplinaDao getDisciplinaDao() {
	return disciplinaDao;
    }

    public List<Disciplina> getDisciplinaList() {
	return disciplinaList;
    }

    public void setDisciplinaList(List<Disciplina> disciplinaList) {
	this.disciplinaList = disciplinaList;
    }

    public void setDisciplina(Disciplina disciplina) {
	this.disciplina = disciplina;
    }

}
