package br.com.martecs.calcnotas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.controller.interfaces.ControllerList;
import br.com.martecs.calcnotas.persistence.daoimp.CursoDisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.CursoDisciplina;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 8 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("cursoDisciplinaController")
public class CursoDisciplinaController implements Serializable, ControllerList {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CursoDisciplinaDao cursoDisciplinaDao;

    private CursoDisciplina cursoDisciplina;

    private List<CursoDisciplina> cursoDisciplinaList;

    @PostConstruct
    @Override
    public void inicializar() {
	list();
    }

    @Override
    public void novo() throws IOException {
	String url = "/calcnotas" + URLConstants.CURSODISCIPLINA_FORM_URL;
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    @Override
    public void editar() throws IOException {
	String url = "/calcnotas" + URLConstants.CURSODISCIPLINA_FORM_URL + "?cursoDisciplina="
		+ cursoDisciplina.getCod() + "&alteracao=true";
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public void copiarCursoDisciplina() throws IOException {
	String url = "/calcnotas" + URLConstants.CURSODISCIPLINA_FORM_URL + "?cursoDisciplina="
		+ cursoDisciplina.getCod() + "&copia=true";
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public void delete() {
	try {
	    cursoDisciplinaDao.delete(cursoDisciplina);
	    list();
	} catch (Exception e) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageController.MSG_EXCL_ERR_INTEGRIDADE, ""));
	    e.printStackTrace();
	    return;
	}
	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage(FacesMessage.SEVERITY_INFO, MessageController.MSG_EXCL_SUCCESS, ""));
    }

    public void list() {
	this.cursoDisciplinaList = cursoDisciplinaDao.getListAll();
    }

    /*----------------GETTERS & SETTERS----------------------*/

    public CursoDisciplina getCursoDisciplina() {
	return cursoDisciplina;
    }

    public CursoDisciplinaDao getCursoDisciplinaDao() {
	return cursoDisciplinaDao;
    }

    public List<CursoDisciplina> getCursoDisciplinaList() {
	return cursoDisciplinaList;
    }

    public void setCursoDisciplina(CursoDisciplina cursoDisciplina) {
	this.cursoDisciplina = cursoDisciplina;
    }

}
