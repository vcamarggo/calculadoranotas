package br.com.martecs.calcnotas.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.enumeration.TipoCombinacao;
import br.com.martecs.calcnotas.persistence.geral.Avaliacao;
import br.com.martecs.calcnotas.persistence.geral.Combinacao;
import br.com.martecs.calcnotas.session.SpringViewScope;

/**
 * @author V.Camargo
 * 
 * @Date 30 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("combinacaoController")
public class CombinacaoController implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Combinacao> combinacoesPossiveis;

    private TipoCombinacao tipoCombinacao = TipoCombinacao.OTIMIZADA;

    private Integer mediaDesejada = 60;

    public void geraCombinacoes(List<Avaliacao> avaliacoes) {
	List<Combinacao> combinacoesSemNota = new ArrayList<>();
	combinacoesPossiveis = new ArrayList<>();
	Double pesoTotal = 0D;
	Double pesoNaoPreenchida = 0D;
	Double notaObtida = 0D;

	for (Avaliacao avaliacao : avaliacoes) {
	    Combinacao c = new Combinacao(avaliacao, avaliacoes.indexOf(avaliacao));
	    if (avaliacao.getNota() != null && avaliacao.getNota() != "") {
		combinacoesPossiveis.add(c);
		notaObtida += avaliacao.getTotal();
	    } else {
		pesoNaoPreenchida += avaliacao.getPeso() * avaliacao.getPorcentagemSub();
		combinacoesSemNota.add(c);
	    }
	    pesoTotal += avaliacao.getPeso() * avaliacao.getPorcentagemSub();
	}
	combinacoesPossiveis.addAll(criaListaFormulaAplicada(combinacoesSemNota, Math.round(pesoTotal),
		pesoNaoPreenchida, Math.round(notaObtida), tipoCombinacao));

	verificaNotaIsAlcancavel(pesoTotal);
	Collections.sort(combinacoesPossiveis);

    }

    private void verificaNotaIsAlcancavel(Double pesoTotal) {
	Double somaNotaSugeridas = 0D;
	for (Combinacao itemCombinacao : combinacoesPossiveis) {
	    somaNotaSugeridas += itemCombinacao.getTotal() / pesoTotal;
	}
	if (Math.round(somaNotaSugeridas) < mediaDesejada) {
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_WARN, MessageController.MSG_NAO_ATINGIRA_NOTA, ""));
	}

    }

    private List<Combinacao> criaListaFormulaAplicada(List<Combinacao> combinacoesSemNota, Long pesoTotal,
	    Double pesoNaoPreenchida, Long notaObtida, TipoCombinacao tipoCombinacao) {
	if (mediaDesejada == null) {
	    mediaDesejada = 60;
	}
	if (tipoCombinacao == null) {
	    tipoCombinacao = TipoCombinacao.OTIMIZADA;
	}
	for (Combinacao itemCombinacao : combinacoesSemNota) {
	    Double resultado = ((mediaDesejada - notaObtida)
		    * ((itemCombinacao.getPeso() * itemCombinacao.getPorcentagemSub()) / pesoNaoPreenchida) * pesoTotal)
		    / (itemCombinacao.getPeso() * itemCombinacao.getPorcentagemSub());
	    if (resultado >= 100) {
		resultado = 100D;
	    } else if (resultado < 0.5) {
		resultado = 0D;
	    }
	    itemCombinacao.setNotaSugerida(Math.round(resultado));
	}
	if (tipoCombinacao == TipoCombinacao.OTIMIZADA && combinacoesSemNota.size() > 1) {

	    Combinacao itemCombinacaoComMaiorPeso = encontraMaiorPeso(combinacoesSemNota);

	    Double notaMaiorPeso = itemCombinacaoComMaiorPeso.getNotaSugerida().doubleValue();
	    Double resultado = notaMaiorPeso + notaMaiorPeso * (1 - (notaMaiorPeso / 100));
	    itemCombinacaoComMaiorPeso.setNotaSugerida(Math.round(resultado));
	    notaObtida += Math.round(Math.floor(itemCombinacaoComMaiorPeso.getTotal() / pesoTotal));
	    combinacoesPossiveis.add(itemCombinacaoComMaiorPeso);
	    combinacoesSemNota.remove(itemCombinacaoComMaiorPeso);
	    pesoNaoPreenchida -= itemCombinacaoComMaiorPeso.getPeso() * itemCombinacaoComMaiorPeso.getPorcentagemSub();
	    combinacoesSemNota = criaListaFormulaAplicada(combinacoesSemNota, pesoTotal, pesoNaoPreenchida, notaObtida,
		    tipoCombinacao);

	}
	return combinacoesSemNota;
    }

    private Combinacao encontraMaiorPeso(List<Combinacao> combinacoesSemNota) {
	Combinacao itemCombinacaoMaiorPeso = combinacoesSemNota.get(0);
	for (Combinacao itemCombinacao : combinacoesSemNota) {
	    if (itemCombinacao.getPeso() * itemCombinacao.getPorcentagemSub() > itemCombinacaoMaiorPeso.getPeso()
		    * itemCombinacaoMaiorPeso.getPorcentagemSub()) {
		itemCombinacaoMaiorPeso = itemCombinacao;
	    }
	}
	return itemCombinacaoMaiorPeso;
    }

    /*
     * System.out.println("Item 3: " + Collections.max(combinacoesSemNota, new
     * Comparator<Combinacao>() {
     * 
     * @Override public int compare(Combinacao o1, Combinacao o2) { return
     * Double.compare((o1.getPeso() * o1.getPorcentagemSub()), (o2.getPeso() *
     * o2.getPorcentagemSub())); } }));
     * 
     * /* Collections.max(combinacoesSemNota, (a, b) -> (a.getPeso() *
     * a.getPorcentagemSub()) > (b.getPeso() * a.getPorcentagemSub()) ? 1 :
     * (a.getPeso() * a.getPorcentagemSub()) > (b.getPeso() *
     * a.getPorcentagemSub()) ? 0 : -1);
     */

    public List<Combinacao> getCombinacoesPossiveis() {
	return combinacoesPossiveis;
    }

    public Integer getMediaDesejada() {
	return mediaDesejada;
    }

    public void setMediaDesejada(Integer mediaDesejada) {
	this.mediaDesejada = mediaDesejada;
    }

    public TipoCombinacao getTipoCombinacao() {
	return tipoCombinacao;
    }

    public void setTipoCombinacao(TipoCombinacao tipoCombinacao) {
	this.tipoCombinacao = tipoCombinacao;
    }

}