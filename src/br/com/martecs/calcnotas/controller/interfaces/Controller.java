package br.com.martecs.calcnotas.controller.interfaces;

/**
 * @author V.Camargo
 * 
 * @Date 7 de abr de 2016
 */

public interface Controller {

    public void inicializar();

}
