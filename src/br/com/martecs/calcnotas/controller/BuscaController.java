package br.com.martecs.calcnotas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.persistence.daoimp.CursoDisciplinaDao;
import br.com.martecs.calcnotas.persistence.geral.CursoDisciplina;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 15 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */

@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("buscaController")
public class BuscaController implements Serializable, br.com.martecs.calcnotas.controller.interfaces.Controller {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CursoDisciplinaDao cursoDisciplinaDao;

    private CursoDisciplina cursoDisciplina;

    private List<CursoDisciplina> cursoDisciplinaList;

    private String keyWordBusca;

    @Override
    public void inicializar() {
	if (keyWordBusca == null || keyWordBusca == "") {
	    cursoDisciplinaList = new ArrayList<>();
	    keyWordBusca = "";
	} else {
	    busca();
	}
    }

    public void abrirCalculadora() throws IOException {
	String url = "/calcnotas" + URLConstants.CALCULADORA_FORM_URL + "?cursoDisciplina=" + cursoDisciplina.getCod();
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public List<String> completa(String keyWord) {
	return cursoDisciplinaDao.filtraPorKeyWord(keyWord);
    }

    public void busca() {
	this.cursoDisciplinaList = cursoDisciplinaDao.getByFilter(keyWordBusca);
    }

    /*----------------GETTERS & SETTERS----------------------*/

    public CursoDisciplinaDao getCursoDisciplinaDao() {
	return cursoDisciplinaDao;
    }

    public CursoDisciplina getCursoDisciplina() {
	return cursoDisciplina;
    }

    public void setCursoDisciplina(CursoDisciplina cursoDisciplina) {
	this.cursoDisciplina = cursoDisciplina;
    }

    public List<CursoDisciplina> getCursoDisciplinaList() {
	return cursoDisciplinaList;
    }

    public void setCursoDisciplinaList(List<CursoDisciplina> cursoDisciplinaList) {
	this.cursoDisciplinaList = cursoDisciplinaList;
    }

    public String getKeyWordBusca() {
	return keyWordBusca;
    }

    public void setKeyWordBusca(String keyWordBusca) {
	this.keyWordBusca = keyWordBusca;
    }

}