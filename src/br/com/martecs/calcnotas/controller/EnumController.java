package br.com.martecs.calcnotas.controller;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import br.com.martecs.calcnotas.enumeration.Autoridade;
import br.com.martecs.calcnotas.enumeration.Departamento;
import br.com.martecs.calcnotas.enumeration.TipoCombinacao;

/**
 * @author V. Camargo
 *
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@Controller("enumController")
public class EnumController implements Serializable {

    private static final long serialVersionUID = 1L;

    public static Departamento[] listDepartamentos() {
	return Departamento.values();
    }

    public static TipoCombinacao[] listTiposCombinacao() {
	return TipoCombinacao.values();
    }

    public static Autoridade[] listAutoridades() {
	return Autoridade.values();
    }

}
