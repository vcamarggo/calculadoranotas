package br.com.martecs.calcnotas.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

/**
 * @author Camargo
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 *
 */
public class MessageController implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static String MSG_EXCL_SUCCESS = "Registro excluído com sucesso!";

    public final static String MSG_EXCL_ERR_INTEGRIDADE = "Exclusão não permitida, falha de integridade!";

    public final static String MSG_INCL_ERR_INTEGRIDADE = "Inclusão não permitida, falha de integridade!";

    public final static String MSG_UPDT_ERR_INTEGRIDADE = "Atualização não permitida, código já registrado!";

    public final static String MSG_SAVE_SUCCESS = "Registro salvo com sucesso!";

    public static final String MSG_REG_DUP = "Inclusão não permitida, registro duplicado.";

    public static final String MSG_AVALIACAO_VAZIA = "É obrigatório a inclusão de avalições!";

    public static final String MSG_SENHA_INCORRETA = "Senha incorreta!";

    public final static String MSG_SENHA_VALID_ERR_SENHA_MINIMO_DIGITOS = "Senha deve ter no mínimo 6 dígitos!";

    public final static String MSG_SENHA_VALID_ERR_CONFIRMACAO = "Confirmação de senha inválida!";

    public final static String MSG_USUARIO_EXISTENTE = "Usuário já existe, login deve ser diferente.";

    public static final String MSG_NAO_ATINGIRA_NOTA = "Você não atingirá a nota selecionada.";

    public static final String MSG_ERRO_AUTENTICACAO = "Houve um erro na autenticação.";
    
    public static void geraMensagem(Severity severity, String mensagem) {
	geraMensagem(severity, mensagem, "");
    }

    public static void geraMensagem(Severity severity, String mensagem, String detail) {
	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, mensagem, detail));
    }

}
