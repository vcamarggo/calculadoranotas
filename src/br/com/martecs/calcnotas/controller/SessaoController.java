package br.com.martecs.calcnotas.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

import br.com.martecs.calcnotas.pessoa.Usuario;

/**
 * @author V.Camargo
 * 
 * @Date 03 de agosto de 2016
 * @category Session
 * 
 */

@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Controller("sessaoController")
public class SessaoController implements Serializable {

    private static final long serialVersionUID = 1L;

    private Usuario usuario;

    public SessaoController() {
    }

    public void atualizaAutorizacao(HttpSession session) {
	try {
	    usuario = (Usuario) session.getAttribute("usuario");
	} catch (SessionAuthenticationException e) {
	    MessageController.geraMensagem(FacesMessage.SEVERITY_ERROR, MessageController.MSG_ERRO_AUTENTICACAO);
	    return;
	}
    }

    public String getNivelAutoridade() {
	if (usuario == null)
	    return null;
	return usuario.getAutoridade().name();
    }

}
