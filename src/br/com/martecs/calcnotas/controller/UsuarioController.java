package br.com.martecs.calcnotas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.controller.interfaces.ControllerList;
import br.com.martecs.calcnotas.persistence.daoimp.UsuarioDao;
import br.com.martecs.calcnotas.pessoa.Usuario;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author Vinícius Camargo
 *
 * @date 14/08/2016
 */
@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("usuarioController")
public class UsuarioController implements Serializable, ControllerList {

    private static final long serialVersionUID = 1L;

    @Autowired
    private UsuarioDao usuarioDao;

    private List<Usuario> usuarioList;

    private Usuario usuario;

    public UsuarioController() {
    }

    @PostConstruct
    public void inicializar() {
	list();
    }

    @Override
    public void novo() throws IOException {
	String url = URLConstants.CONTEXT + URLConstants.USUARIO_FORM_URL;
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    @Override
    public void editar() throws IOException {
	String url = URLConstants.CONTEXT + URLConstants.USUARIO_FORM_URL + "?usuario=" + this.usuario.getId();
	FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public void delete() {
	try {
	    usuarioDao.delete(usuario);
	    list();
	} catch (Exception e) {
	    MessageController.geraMensagem(FacesMessage.SEVERITY_ERROR, MessageController.MSG_EXCL_ERR_INTEGRIDADE);
	    e.printStackTrace();
	    return;
	}
	MessageController.geraMensagem(FacesMessage.SEVERITY_INFO, MessageController.MSG_EXCL_SUCCESS);
    }

    public List<Usuario> list() {
	return usuarioList = usuarioDao.getListAll();
    }

    /*----------------GETTERS & SETTERS----------------------*/

    public List<Usuario> getUsuarioList() {
	return usuarioList;
    }

    public Usuario getUsuario() {
	return usuario;
    }

    public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
    }

}
