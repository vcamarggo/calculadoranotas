package br.com.martecs.calcnotas.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.martecs.calcnotas.persistence.geral.Avaliacao;
import br.com.martecs.calcnotas.persistence.geral.CursoDisciplina;
import br.com.martecs.calcnotas.session.SpringViewScope;
import br.com.martecs.calcnotas.web.constants.URLConstants;

/**
 * @author V.Camargo
 * 
 * @Date 16 de mar de 2016
 * 
 * @License This file is part of calcnotas. calcnotas is free software: you can
 *          redistribute it and/or modify it under the terms of the GNU General
 *          Public License as published by the Free Software Foundation, either
 *          version 3 of the License, or (at your option) any later version.
 *          calcnotas is distributed in the hope that it will be useful, but
 *          WITHOUT ANY WARRANTY; without even the implied warranty of
 *          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *          General Public License for more details. You should have received a
 *          copy of the GNU General Public License along with calcnotas. If not,
 *          see http://www.gnu.org/licenses/.
 */
@Scope(SpringViewScope.VIEW_SCOPE_KEY)
@Controller("calculadoraController")
public class CalculadoraController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CombinacaoController combinacaoController;

    private Integer codDisciplina;

    private CursoDisciplina cursoDisciplina;

    private Avaliacao avaliacao;

    private List<Avaliacao> avaliacoes;

    private String keyWordBusca;

    private Long totalGeral;

    private Boolean fimCalculos = false;

    private Long totalExame;

    private Boolean showOpcoes = false;

    @Autowired
    private BuscaController buscaController;

    private String statusDisciplina;

    public void inicializar() throws IOException {
	//cursoDisciplina = cursoDisciplinaDaoImpl.getByCod(codDisciplina);
	try {
	    avaliacoes = cursoDisciplina.getAvaliacoes();
	} catch (NullPointerException e) {
	    String url = "/calcnotas/" + URLConstants.HOME_URL;
	    FacesContext.getCurrentInstance().getExternalContext().redirect(url);
	}
    }

    public List<String> completa(String keyWord) {
	return buscaController.completa(keyWord);
    }

    public void busca() throws IOException {
	try {
	    String url = "/calcnotas/" + URLConstants.HOME_URL + "?keyWordBusca=" + keyWordBusca;
	    FacesContext.getCurrentInstance().getExternalContext().redirect(url);
	} catch (IOException e) {
	    String url = "/calcnotas" + URLConstants.CALCULADORA_FORM_URL;
	    FacesContext.getCurrentInstance().getExternalContext().redirect(url);
	}
    }

    public void calculaTotalGeral() {
	Double totalGeral = 0D;
	int numeroAvaliacoes = 0;
	fimCalculos = false;
	for (Avaliacao avaliacao : avaliacoes) {
	    totalGeral += avaliacao.getTotal();
	    if (avaliacao.getNota() != null && avaliacao.getNota() != "") {
		numeroAvaliacoes++;
	    }
	}
	this.totalGeral = Math.round(totalGeral);
	if (this.totalGeral >= 60) {
	    statusDisciplina = "Parabéns! Você não precisa de exame";
	    totalExame = null;
	} else if (numeroAvaliacoes == avaliacoes.size()) {
	    statusDisciplina = "Nota mínima necessária para passar no exame";
	    calculaNotaParaExame();
	    fimCalculos = true;
	} else {
	    statusDisciplina = "";
	}
    }

    private void calculaNotaParaExame() {
	if (totalGeral == null) {
	    totalGeral = 0L;
	} else if (totalGeral > 60) {
	    totalExame = 0L;
	}
	totalExame = (long) Math.round((50 - ((double) totalGeral / 2)) * 2);
    }

    public void geraCombinacao() {
	combinacaoController.geraCombinacoes(avaliacoes);
    }

    public void showOpcoes() {
	showOpcoes = !showOpcoes;
    }

    public CursoDisciplina getCursoDisciplina() {
	return cursoDisciplina;
    }

    public void setCursoDisciplina(CursoDisciplina cursoDisciplina) {
	this.cursoDisciplina = cursoDisciplina;
    }

    public Avaliacao getAvaliacao() {
	return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
	this.avaliacao = avaliacao;
    }

    public String getKeyWordBusca() {
	return keyWordBusca;
    }

    public void setKeyWordBusca(String keyWordBusca) {
	this.keyWordBusca = keyWordBusca;
    }

    public List<Avaliacao> getAvaliacoes() {
	return avaliacoes;
    }

    public BuscaController getBuscaController() {
	return buscaController;
    }

    public Long getTotalGeral() {
	return totalGeral;
    }

    public Long getTotalExame() {
	return totalExame;
    }

    public String getStatusDisciplina() {
	return statusDisciplina;
    }

    public Boolean getFimCalculos() {
	return fimCalculos;
    }

    public Boolean getShowOpcoes() {
	return showOpcoes;
    }

    public Integer getCodDisciplina() {
	return codDisciplina;
    }

    public void setCodDisciplina(Integer codDisciplina) {
	this.codDisciplina = codDisciplina;
    }

}
